from setuptools import setup, find_packages

setup(name='Monitoria API',
      packages=find_packages(),
      setup_requires=['setuptools_scm', 'setuptools', 'Cython'],
      install_requires=[
          "Django==3.1",
          "django-filter==2.4.0",
          "djangorestframework==3.12.4",
          "django-cors-headers==3.8.0",
          "psycopg2-binary==2.9.3",
      ],
      extras_require={
          'dev': [
              'flake8==3.9.2'
          ]
      },
      include_package_data=True,
      author='Luiz Fernando',
      author_email='luizfernandorole@sempreceub.com')
