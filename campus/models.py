from django.db import models

# Create your models here.
class Campus(models.Model):
    local = models.CharField(max_length=100, unique=True)
    nome = models.CharField(max_length=100, unique=True)
