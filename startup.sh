#!/bin/sh

# Migrate model changes
python3 /api/manage.py migrate

python3 /api/manage.py runserver 0.0.0.0:8000
