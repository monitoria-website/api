from django.db import models
from campus.models import Campus


# Create your models here.
class Monitor(models.Model):
    nome = models.CharField(max_length=40)
    email = models.EmailField(null=False)
    campus = models.ForeignKey(Campus, on_delete=models.CASCADE)
    numero = models.CharField(max_length=27)
