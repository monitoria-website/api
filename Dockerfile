FROM python:3.9.10-slim-buster

COPY . ./api
WORKDIR /api

# build application dependencies
RUN pip install --upgrade pip

RUN pip install  -e . --no-cache-dir
