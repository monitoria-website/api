from django.db import models

# Create your models here.
class Curso(models.Model):
    nome = models.CharField(max_length=200, unique=True)
    inicio = models.DateField(null=True)
    fim = models.DateField(null=True)
    horario = models.TimeField(null=True)
