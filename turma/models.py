from django.db import models
from campus.models import Campus
from monitor.models import Monitor
from curso.models import Curso


# Create your models here.
class Turma(models.Model):
    inicio = models.DateField(null=False)
    fim = models.DateField(null=False)
    hora_inicio = models.TimeField(null=False)
    hora_fim = models.TimeField(null=False)
    inscricao_inicio = models.DateField(null=False)
    inscricao_fim = models.DateField(null=False)
    link_aula = models.CharField(max_length=100)
    link_inscricao = models.CharField(max_length=100)
    local = models.ManyToManyField(Campus)
    monitor = models.ManyToManyField(Monitor)
    curso = models.ForeignKey(Curso, on_delete=models.CASCADE)
