# Uso
***

## Com Docker
- Voce vai precisar clonar o repositorio localmente.
- Execute no terminal `cd api`
- E rode o comando `docker-compose up --build api`
- A Api vai subir com suas dependencias.

> Nota: A Api depende de um servico de postgress para poder aplicar as migracoes
e executar as funcionalidades de CRUD no banco.